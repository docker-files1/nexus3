# [1.8.0](/Docker_Files/nexus3/compare/1.8.0...master)
* CICD migraton from Jenkins to GitlabCI

# [1.7.0](/Docker_Files/nexus3/compare/1.7.0...master)
* Docker linter apply and little hotfix that might affect to some files that match with the current `.gitignore`

# [1.6.0](/Docker_Files/nexus3/compare/1.6.0...master)
* Se actualiza el formato de la documentación
* Se actualiza el repositorio de *Debian strech*

# [1.5.0](/Docker_Files/nexus3/compare/1.5.0...master)
* Se actualiza el jenkinsfile segun la nueva versión de la *Shared Library*
* Se retira soporte para armv7

# [1.4.1](/Docker_Files/nexus3/compare/1.4.1...master)
* Se ajusta el `Jenkinsfile` con nueva sintaxis para listados

# [1.4.0](/Docker_Files/nexus3/compare/1.4.0...master)
* Se ajusta el `Jenkinsfile` con nuevas reglas de validación

# [1.3.0](/Docker_Files/nexus3/compare/1.3.0...master)
* Se ajusta el `README` para que muestre información relevante
* Se ajusta el `Jenkinsfile` para informar sobre el path donde se encuentra las `Shared libs`
* Se eliminan del `Dockerfile`, las etiquetas genéricas ya que son insertadas al momento de hacer el build de manera automática.
* Se actualiza la configuración por defecto del *.editorconfig* y el *.gitignore*

# [1.2.0](/Docker_Files/nexus3/compare/1.2.0...master)
* Se simplifican los pipelines usando los estándares segun tipologia, para el caso `dockerfilePipeline`
    * Se eliminan del Dockerfile, las etiquetas genéricas ya que son insertadas al momento de hacer el build de manera automática.

# [1.1.1](/Docker_Files/nexus3/compare/1.1.1...master)
* Se evaluaba de manera incorrecta la instalación, esto provocaba que se de por hecho que está instalado el aplicativo aunque no lo esté si se usa volumen persistente para los datos.
* Se ajusta la receta de jenkins para que haga build sobre las plataformas `linux/amd64, linux/arm/v7`

# [1.1.0](/Docker_Files/nexus3/compare/1.1.0...master)
* Actualización de notas en Dockerfile para generación de imagenes para `linux/arm64/v8`
* Cambio de repositorio para traer el tar de instalación NEXUS3
    * Resumen de instalación muestra versión de NEXUS

# [1.0.0](/Docker_Files/nexus3/compare/1.0.0...master)
* Inicialización del `CHANGELOG`
* Versión inicial del dockerfile
