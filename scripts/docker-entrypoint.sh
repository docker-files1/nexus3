#!/bin/bash
set -e

if [ "$1" = "nexus" ]; then
	NEXUSDIR="/opt/nexus/"
	NEXUSTMPDIR="/var/tmp/"
	# NEXUSREPOSITORY="https://sonatype-download.global.ssl.fastly.net/repository/downloads-prod-group/3/"
	NEXUSREPOSITORY="https://docker.arroyof.com/downloads/"
	NEXUSVERSION="3.41.1-01"
	NEXUSPACKAGE="nexus-${NEXUSVERSION}-unix.tar.gz"
	NEXUSUSER="nexus"

	if [ -z "$MINMEMORY" ]; then
		export MINMEMORY="256"
	fi

	if [ -z "$MAXMEMORY" ]; then
		export MAXMEMORY="768"
	fi

	if [ ! -x "${NEXUSDIR}bin/nexus" ]; then
		# NEXUS user creation / configuration
		useradd -m -d "$NEXUSDIR" -s /bin/bash -r "$NEXUSUSER" &> /dev/null && echo "NEXUS user creation [ OK ]" || exit 2
		wget -O /var/tmp/"$NEXUSPACKAGE" "$NEXUSREPOSITORY$NEXUSPACKAGE" &> /dev/null && echo "NEXUS tarball download [ OK ]" || exit 2
		# NEXUS unpackage tarball
		tar xzf /var/tmp/"$NEXUSPACKAGE" -C "$NEXUSDIR" --strip-components=1 && chown -R "$NEXUSUSER": "$NEXUSDIR" &> /dev/null && echo "NEXUS files ready [ OK ]" || exit 2

		sed "s/MINMEMORY_CUSTOM/$MINMEMORY/g" "${NEXUSTMPDIR}nexus.vmoptions" > "${NEXUSDIR}bin/nexus.vmoptions"
		sed -i "s/MAXMEMORY_CUSTOM/$MAXMEMORY/g" "${NEXUSDIR}bin/nexus.vmoptions"
		sed "s/NEXUSUSER_CUSTOM/$NEXUSUSER/g" "${NEXUSTMPDIR}nexus.rc" > "${NEXUSDIR}bin/nexus.rc"
	fi

cat << EOB

****************************************************
*                                                  *
*    Docker image: oscarenzo/nexus3                *
*    https://gitlab.com/docker-files1/nexus3       *
*                                                  *
****************************************************

SERVER SETTINGS
---------------
· NEXUS version: $NEXUSVERSION
· NEXUS user: $NEXUSUSER
· NEXUS directory: $NEXUSDIR
· JAVA minimal memory: $MINMEMORY
· JAVA maximun memor: $MAXMEMORY
---------------

EOB

	set -- "${NEXUSDIR}bin/nexus" "run"
fi

exec "$@"
