[![pipeline status](https://gitlab.com/docker-files1/nexus3/badges/development/pipeline.svg)](https://gitlab.com/docker-files1/nexus3/-/commits/development) ![Project version](https://img.shields.io/docker/v/oscarenzo/nexus3?sort=date) ![Docker image pulls](https://img.shields.io/docker/pulls/oscarenzo/nexus3) ![Docker image size](https://img.shields.io/docker/image-size/oscarenzo/nexus3?sort=date) ![Project license](https://img.shields.io/gitlab/license/docker-files1/nexus3)

# nexus3
Dockerfile to create containers with Nexus repository.

## 🧾 Components
 - Debian
 - Sonatype nexus 3.41.1
 - Openjdk 8

## 🔗 References
https://kifarunix.com/install-nexus-repository-manager-on-debian-11/

## ✍️ Advices
Once the container is running, you need to login it and run this command to check the default `admin` password:

```
cat /opt/nexus/sonatype-work/nexus3/admin.password
```

For persistent volume configuration you may think on this paths:

```
/opt/nexus/sonatype-work/nexus3/
```

## ⚙️ Environment variables
This image uses environment variables to allow the configuration of some parameters at run time:

* Variable name: `MINMEMORY`
* Default value: 256
* Accepted values: Int.
* Description: These value is specified as Mb for minimal java memory.

----

* Variable name: `MAXMEMORY`
* Default value: 768
* Accepted values: Int.
* Description: These value is specified as Mb for maximun java memory.

## 💬 Legend
* NKS => No key sensitive
* KS => Key sensitive
