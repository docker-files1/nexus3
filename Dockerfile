FROM debian:buster-slim

LABEL DistBase="Debian 10 - Buster"

RUN echo "deb http://archive.debian.org/debian-security stretch/updates main" > /etc/apt/sources.list.d/java-8.list && \
	apt-get update -y && \
	apt-get install -y netcat wget vim openjdk-8-jre-headless --no-install-recommends && \
	apt-get autoremove -y && apt-get clean -y && \
	rm -rf /var/lib/apt/lists/* /etc/apt/sources.list.d/java-8.list

# NEXUS pre-configurations
COPY config/nexus.vmoptions /var/tmp/
COPY config/nexus.rc /var/tmp/
COPY scripts/docker-entrypoint.sh /var/tmp/

RUN chmod +x /var/tmp/docker-entrypoint.sh

ENTRYPOINT ["/var/tmp/docker-entrypoint.sh"]

EXPOSE 8081/tcp

HEALTHCHECK --interval=5m --timeout=3s \
  CMD nc -z localhost 8081 || exit 1

CMD ["nexus"]
# docker build -t oscarenzo/nexus3:latest .
# docker buildx build --push -t oscarenzo/nexus3:latest --platform linux/amd64,linux/arm/v7,linux/arm64/v8 .
